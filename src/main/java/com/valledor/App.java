package com.valledor;

import java.util.Scanner;
import static java.lang.System.*;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        out.println("Complete Name:");
        scanner.nextLine();

        out.print("Age: ");
        scanner.nextInt();

        out.print("\n1st Grading: ");
        int firstGrading = scanner.nextInt();

        out.print("2nd Grading: ");
        int secondGrading = scanner.nextInt();

        out.print("3rd Grading: ");
        int thirdGrading = scanner.nextInt();

        out.print("4th Grading: ");
        int fourthGrading = scanner.nextInt();

        int average = (firstGrading + secondGrading + thirdGrading + fourthGrading) / 4;
        out.print("\nAverage: " + average);
    }
}
